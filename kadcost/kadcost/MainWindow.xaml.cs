﻿using System.Collections.Generic;
using System.Windows;
using kadcost.Math;
using MathNet.Numerics.LinearAlgebra;
using IronPython.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace kadcost
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string user = "I.mr.3.14%40yandex.ru";
        private const string token = "bc349ec35820eea2fe80d0b9f2d97bfa";
        private static readonly string prefix = $"http://ads-api.ru/main/api?user={user}&token={token}";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonLoadAnalogues_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(TbCity.Text) && !string.IsNullOrEmpty(TbArea.Text) &&
                !string.IsNullOrEmpty(TbRoof.Text) && !string.IsNullOrEmpty(TbRooms.Text))
            {
                var dict = new Dictionary<string, string>();
                dict.Add("city", TbCity.Text);
                dict.Add("area", TbArea.Text);
                dict.Add("roof", TbRoof.Text);
                dict.Add("rooms", TbRooms.Text);
                string request = Connection.Instance.GET(prefix + "&" + $"city={TbCity.Text}" + "&" + "limit=4" + "&" +
                                                         "category_id=2" + "&" + "param[2113]=2");
                dynamic deserializeObject = JsonConvert.DeserializeObject(request);
                Matrix<double> mass = Cost.Instance.Init(deserializeObject, dict);
                // площадь 'param_2836': '18.5', этаж 'param_2636': 2, этажей в доме 'param_2736': 4,
                // комнат в квартире 'param_2545': 6,  координаты 'coords': {'lat': '55.761674', 'lng': '37.616027'}
                double count = System.Math.Round(mass[0,0])  > 0?System.Math.Round(mass[0,0]): System.Math.Round(mass[0,0])*-1; 
                double area = System.Math.Round(mass[1,0])  > 0?System.Math.Round(mass[1,0]): System.Math.Round(mass[1,0])*-1; 
                double room = System.Math.Round(mass[2,0])  > 0?System.Math.Round(mass[2,0]): System.Math.Round(mass[2,0])*-1; 
                double roof = System.Math.Round(mass[3,0])  > 0?System.Math.Round(mass[3,0]): System.Math.Round(mass[3,0])*-1; 
                 
                Count.Content = count.ToString();
                LbAreaPerOne.Content = area.ToString();
                LbRoomPerOne.Content = room.ToString();
                LbRoofPerOne.Content = roof.ToString();
            }
            else
            {
                MessageBox.Show("Ошибка формата вводимых данных. Убедитесь что исходные данные введены верно");
                TbArea.Clear();
                TbCity.Clear();
                TbRoof.Clear();
                TbRooms.Clear();
            }
        }

        private void ButtonCalculateCost_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}