﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace kadcost.Math
{
    public class Cost
    {
        private static Cost _instance;
        public static Cost Instance => _instance ?? (_instance = new Cost());

        public Matrix<double> Init(dynamic requestFromServer, Dictionary<string, string> dict)
        {
            Matrix<double> originalMatrix = FormOriginalMatrix(requestFromServer, dict);
            Matrix<double> pMatrix = FormPMatrix(requestFromServer);
            Matrix<double> deltaXMatrix = GetDeltaXMatrix(originalMatrix);
            return GetVMatrix(deltaXMatrix, pMatrix);
        }

        private Matrix<double> FormOriginalMatrix(dynamic requestFromServer, Dictionary<string, string> dict)
        {
            var mass = new double[,]
            {
                {
                    Convert.ToDouble(dict["area"]),
                    Convert.ToDouble(dict["rooms"]),
                    Convert.ToDouble(dict["roof"])
                },
                {
                    (double) requestFromServer.data[0].param_2313,
                    (double) requestFromServer.data[0].param_1945,
                    (double) requestFromServer.data[0].param_2213
                },
                {
                    (double) requestFromServer.data[1].param_2313,
                    (double) requestFromServer.data[1].param_1945,
                    (double) requestFromServer.data[1].param_2213
                },
                {
                    (double) requestFromServer.data[2].param_2313,
                    (double) requestFromServer.data[2].param_1945,
                    (double) requestFromServer.data[2].param_2213
                },
                {
                    (double) requestFromServer.data[3].param_2313,
                    (double) requestFromServer.data[3].param_1945,
                    (double) requestFromServer.data[3].param_2213
                },
            };
            return DenseMatrix.OfArray(mass);
        }

        private Matrix<double> FormPMatrix(dynamic requestFromServer)
        {
            var mass = new double[4, 1]
            {
                {(double) requestFromServer.data[0].price},
                {(double) requestFromServer.data[1].price},
                {(double) requestFromServer.data[2].price},
                {(double) requestFromServer.data[3].price},
            };
            return DenseMatrix.OfArray(mass);
        }

        private Matrix<double> GetDeltaXMatrix(Matrix<double> original)
        {
            Matrix<double> deltaXMatrix = DenseMatrix.Create(original.RowCount - 1, original.ColumnCount + 1, 0);
            for (int i = 0; i < deltaXMatrix.RowCount; i++)
                deltaXMatrix[i, 0] = 1;

            for (int Column = 1; Column < deltaXMatrix.ColumnCount; Column++)
            {
                for (int Row = 0; Row < deltaXMatrix.RowCount; Row++)
                {
                    deltaXMatrix[Row, Column] = original[Row + 1, Column - 1] - original[0, Column - 1];
                }
            }

            return deltaXMatrix;
        }

        private Matrix<double> GetVMatrix(Matrix<double> deltaXMatrix, Matrix<double> pMatrix)
        {
            return deltaXMatrix.Inverse() * pMatrix;
        }
    }
}