using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace kadcost.Math
{
    public class Cost
    {
        private static Cost _instance;
        public static Cost Instance => _instance ?? (_instance = new Cost());

        public Matrix<double> Init()
        {
            Matrix<double> originalMatrix = FormOriginalMatrix();
            Matrix<double> pMatrix  = FormPMatrix();
            Matrix<double> deltaXMatrix = GetDeltaXMatrix(originalMatrix);
            return GetVMatrix(deltaXMatrix, pMatrix);
        }

        private Matrix<double> FormOriginalMatrix()
        {
            return DenseMatrix.OfArray(new double[,]
            {
                {4, 1, 1},
                {7, 1, 1},
                {3, 1, 0},
                {4, 0, 0},
                {1, 1, 1}
            });
        }

        private Matrix<double> FormPMatrix()
        {
            return DenseMatrix.OfArray(new double[,]
            {
                {14584},
                {10000},
                {8750},
                {10417},
            });
        }

        private Matrix<double> GetDeltaXMatrix(Matrix<double> original)
        {
            Matrix<double> deltaXMatrix = DenseMatrix.Create(original.RowCount - 1, original.ColumnCount + 1, 0);
            for (int i = 0; i < deltaXMatrix.RowCount; i++)
                deltaXMatrix[i, 0] = 1;
            
            for (int Column = 1; Column < deltaXMatrix.ColumnCount; Column++)
            {
                for (int Row = 0; Row < deltaXMatrix.RowCount; Row++)
                {
                    deltaXMatrix[Row, Column] = original[Row + 1 , Column - 1] - original[0, Column - 1];
                }
            }
            return deltaXMatrix;
        }

        private Matrix<double> GetVMatrix(Matrix<double> deltaXMatrix, Matrix<double> pMatrix)
        {
            return deltaXMatrix.Inverse() * pMatrix;
        }

        
    }
}