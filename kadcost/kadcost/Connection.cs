using kadcost.Math;

namespace kadcost
{
    public class Connection
    {

        private static Connection _instance;
        public static Connection Instance => _instance ?? (_instance = new Connection());
        
        public string GET(string Url)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(Url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.Stream stream = resp.GetResponseStream();
            System.IO.StreamReader sr = new System.IO.StreamReader(stream);
            string Out = sr.ReadToEnd();
            sr.Close();
            return Out;
        }
    }
}